# Sopa Binaria

Una sopa binaria es un tipo especial de matriz de bits donde se pueden representar números decimales con su correspondiente conversación binaria.


El objetivo del ejercicio es crear un programa que lea una matriz binaria almacenada en un archivo de Excel, un numero decimal  y encuentre la cantidad de veces que está el número(en su representación binaria) en la matriz.

#### Requrimientos

Los resultados deben estar presentados en un documento pdf donde muestre subrayado o resaltado los números encontrados. 

Su aplicación debe contar una GUI realizada con la tecnología que a bien considere, se sugiere por facilidad de uso java.awt o java.swing.


[Descripción completa](./docs/Actividad-2-SOPA-BINARIA-II-sem-2020-Estructuras-de-datos.pdf)


#### Requisitos antes de ejecutar

Configurar en tu entorno las liberías .jar encontradas en la raíz del proyecto.


#### Versiones:

* Apache NetBeans IDE 11.3
* Java(TM) SE Development Kit 11.0.8 

Made with ♥ by [Jose Florez](www.joseflorez.co)
