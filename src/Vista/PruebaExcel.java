/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.SopaBinaria;
import java.io.IOException;

/**
 * Clase de prueba para archivo estático de excel con la matriz binaria
 * @author Jose Florez
 */
public class PruebaExcel {
    
    public static void main(String[] args) throws IOException {
        try {
            SopaBinaria miExcel=new SopaBinaria("src/Datos/sopa.xls",0);
            miExcel.crearPDF(17);
        }catch (Exception e) {
            System.out.println(e.getMessage());
            // e.printStackTrace();
        }
    }
}
