/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author flore
 */
public class JavaFXView extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
 
        Scene scene = new Scene(root);
 
        stage.setScene(scene);
        System.out.println(stage);
        stage.show();
    }
 
    public static void main(String[] args) {
        launch(args);
    }
}
