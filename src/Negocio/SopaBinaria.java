/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Bit;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Contiene toda la lógica agregar los datos del excel y hacer las busquedas del
 * número binario
 *
 * @author Jose Florez
 */
public class SopaBinaria {

    private Bit mySopaBinaria[][];

    public SopaBinaria() {
    }

    public SopaBinaria(String rutaArchivoExcel, int numHoja) throws IOException {
        this.leerArchivo(rutaArchivoExcel, numHoja);
    }

    /**
     * Leemos el archivo y lo cargamos a la matriz
     *
     * @param ruta: el archivo que vamos a leer
     * @param numHoja: número de hoja del archivo
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void leerArchivo(String ruta, int numHoja) throws FileNotFoundException, IOException {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(ruta));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(numHoja);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum() + 1;
        //Creo las filas para la matriz resultante:
        this.mySopaBinaria = new Bit[canFilas][];
        //Recorro cada una de las filas con sus correspondientes columnas
        int columnasIguales = 0;
        for (int i = 0; i < canFilas; i++) {
            //Obtengo el vector asociado a esa Fila (contiene sus columnas)
            HSSFRow filas = hoja.getRow(i);
            int cantCol = filas.getLastCellNum();
            if (i != 0 && columnasIguales != cantCol) {
                throw new UnsupportedOperationException("Error, La matriz debe ser cuadrada o rectagular. Error en Fila " + (i + 1));
            } else {
                columnasIguales = cantCol;
                this.mySopaBinaria[i] = new Bit[cantCol];
                for (int j = 0; j < cantCol; j++) {
                    // elemento i, j
                    // El formato del excel = Numérico
                    if (filas.getCell(j) != null) {
                        int num = (int) Double.parseDouble(filas.getCell(j).getNumericCellValue() + "");
                        boolean n = num == 1;
                        Bit valor = new Bit(n);
                        this.mySopaBinaria[i][j] = valor;
                    } else {
                        String error = "Error en Fila " + (i + 1) + " columna " + (j + 1);
                        throw new UnsupportedOperationException("Error, La matriz debe estar completamente llena. " + error);
                    }
                }
            }
        }
    }

    /**
     * Verifica si el número binario es palindrome. Esto se hace para hacer solo
     * una búsqueda en una dirección
     *
     * @param str: número binario a examinar
     * @return si es o no palíndrome
     */
    static boolean isPalindrome(String str) {
        // Punteros que apuntan al principio y al final de la cadena
        int i = 0, j = str.length() - 1;
        // Siempre que i sea menor que j repetir
        while (i < j) {
            // Si no son iguales no es palindrome
            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            // Incrementamos i y disminuimos j
            i++;
            j--;
        }
        // si termina el ciclo y no ha dado falso es porque es palindome.
        return true;
    }
    
    /**
     * Busca a partir de la posición j, el binario iterando sobre él. 
     * Se hace al mismo tiempo hacia adelante y hacia atrás para optimizar recursos
     * 
     * @param sopa: Array de Bits que contiene la fila
     * @param binario: String del binario a buscar
     * @param j: la posición de iteracción de la matriz principal
     * @param esPalindrome: Boolean para determinar si hace la búsqueda en ambas direcciones
     * búsqueda.
     */
    private int buscarIzquierdaDerechaViceversa(Bit []sopa, String binario, int j, boolean esPalindrome){
        int cant = 0;
        boolean coincidenciaIzDe = true;
        boolean coincidenciaDeIz = true;
        // Por cada posición {j} iteramos sobre el binario para buscar coincidencias.
        for (int n = 0; (coincidenciaIzDe || coincidenciaDeIz) && n < binario.length(); n++) {
            // IZQUIERA A DERECHA
            if(coincidenciaIzDe) {
                // Buscamos valores después de {j} hasta {n} de binario.lenght
                int valorM = sopa[j + n].getValor() == true ? 1 : 0;
                if (valorM != Integer.parseInt(binario.charAt(n) + "")) {
                    // Si durante el ciclo de binario no coincide rompemos el ciclo.
                    coincidenciaIzDe = false;
                }
            }

            // DERECHA A IZQUIERA
            if(coincidenciaDeIz && !esPalindrome){
                // Iteramos hacia atrás en j, creo una posición j inversa, desde la última posición a la primera.
                int _j = sopa.length - 1 - j;
                int valorMInverso = sopa[_j - n].getValor() == true ? 1 : 0;
                if (valorMInverso != Integer.parseInt(binario.charAt(n) + "")) {
                    coincidenciaDeIz = false;
                }   
            }
        }
        // si durante la iteracción del binario se mantuvo true, cuenta como una coincidencia
        if (coincidenciaIzDe) {
            cant++;
            // Al encontrarse una coincidencia iteramos para colorear.
            for (int n = 0; n < binario.length(); n++) {
                sopa[j + n].setColor(true);
            }
        }
        if (coincidenciaDeIz && !esPalindrome) {
            cant++;
            // Al encontrarse una coincidencia iteramos para colorear.
            for (int n = 0; n < binario.length(); n++) {
                int _j = sopa.length - 1 - j;
                sopa[_j - n].setColor(true);
            }
        }
        return cant;
    }
    
    /**
     * Busca el número convertido a binario N veces horizontalmente de izquiera
     * a derecha y viceversa.
     *
     * @param matriz: Matriz donde vamos a buscar el número binario
     * @param numero: Entero que convertiremos a binario para realizar la
     * búsqueda.
     * @return la cantidad de veces encontrado el binario en la matriz
     */
    private int buscarEnMatrizHorizontal(Bit[][] matriz, int numero) {
        // Convertimos a binario el número recibido.
        String binario = this.convertirEnteroABinario(numero);
        // Verificamos si es palíndrome para solo realizar una sóla búsqueda en una dirección.
        boolean esPalindrome = isPalindrome(binario);
        // contador para almacenar las veces encontrado el binario en la matriz
        int cant = 0;
        // Iteramos sobre el objeto al no necesitar la posición {i}
        for (Bit[] sopa : matriz) {
            // Validamos que el excel en la posición N sea de igual o mayor tamaño que el binario
            if (sopa.length >= binario.length()) {
                // Iteramos sobre cada una de las columnas o posiciones de la fila para comparar con el binario.
                for (int j = 0; j < sopa.length; j++) {
                    // Verificamos que aún quedan suficientes posiciones para seguir buscando.
                    // es decir, si el binario.length = 5, y la matriz de 8, después de la posición 4 no tiene sentido seguir.
                    int limite = sopa.length - j;
                    if (limite >= binario.length()) {
                        // Buscamos en ambas direcciones
                        cant += this.buscarIzquierdaDerechaViceversa(sopa, binario, j, esPalindrome);
                    }
                }
            }
        }
        return cant;
    }


    /**
     * Busca dentro de la matriz cuantas veces está el número en binario de
     * izquiera a derecha y viceversa.
     *
     * @param numero: Valor a buscar
     * @return cantidad de veces encontrado el número de binario en la matriz.
     */
    public int getCuantasVeces_Horizontal(int numero) {
        return this.buscarEnMatrizHorizontal(this.mySopaBinaria, numero);
    }

    /**
     * Busca dentro de la matriz cuantas veces está el número en binario de
     * arriba hacia abajo y viceversa.
     *
     * @param numero: Valor a buscar
     * @return cantidad de veces encontrado el número de binario en la matriz.
     */
    public int getCuantasVeces_Vertical(int numero) {
        // Buscar vertical es igual que horizontal, por ello solo
        // creo una matriz temporal invertida y llamo al método que usa el horizontal
        Bit[][] mySopaTemp = new Bit[this.mySopaBinaria[0].length][this.mySopaBinaria.length];
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            for (int j = 0; j < this.mySopaBinaria[i].length; j++) {
                // En la posición i colocamos j para invertir y viceversa y asignamos de la forma normal {i}{j}
                mySopaTemp[j][i] = this.mySopaBinaria[i][j];
            }
        }
        return this.buscarEnMatrizHorizontal(mySopaTemp, numero);
    }

    /**
     * Busca el número convertido a binario N veces vertical de {abajo, arriba}
     * - {izquiera a derecha} y viceversa.
     *
     * @param matriz: Matriz donde vamos a buscar el número binario
     * @param numero: Entero que convertiremos a binario para realizar la
     * búsqueda.
     * @return la cantidad de veces encontrado el binario en la matriz
     */
    private int getCantidadDiagonal(Bit[][] matriz, int numero) {
        // Convertimos a binario el número recibido.
        String binario = this.convertirEnteroABinario(numero);
        // Verificamos si es palíndrome para solo realizar una sóla búsqueda en una dirección.
        boolean esPalindrome = isPalindrome(binario);
        // contador para almacenar las veces encontrado el binario en la matriz
        int cant = 0;
        // Iteramos con i porque necesitamos las posiciones para realizar las búsquedas.
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                /**
                 * El límite es para determinar que aún quedan suficientes
                 * posiciones para seguir buscando, es decir, si el
                 * binario.length = 5, y la matriz de 8, después de la posición
                 * 4 no debemos seguir si no tiene caso.
                 *
                 * En este caso, el límite va en ambas direcciones, {i,j}
                 */
                int limite_i = matriz.length - i;
                int limite_j = matriz[i].length - j;

                if (limite_i >= binario.length() && limite_j >= binario.length()) {
                    // (DIAGONAL) = IZQUIERDA A DERECHA -> BAJANDO
                    boolean coincidenciaIzDe = true;
                    for (int n = 0; coincidenciaIzDe && n < binario.length(); n++) {
                        // para los valores diagonales les sumanos en {i} y {j} {n}
                        int valorM = matriz[i + n][j + n].getValor() == true ? 1 : 0;
                        if (valorM != Integer.parseInt(binario.charAt(n) + "")) {
                            // Si durante el ciclo de binary no coincide rompemos el ciclo.
                            coincidenciaIzDe = false;
                        }
                    }
                    if (coincidenciaIzDe) {
                        cant++;
                        for (int n = 0; coincidenciaIzDe && n < binario.length(); n++) {
                            // Al encontrarse una coincidencia iteramos para colorear.
                            matriz[i + n][j + n].setColor(true);
                        }
                    }
                    // (DIAGONAL) = DERECHA A IZQUIERA -> SUBIENDO
                    // Solo se busca si no es palindrome
                    if (!esPalindrome) {
                        boolean coincidenciaDeIz = true;
                        // Iteramos hacia atrás
                        for (int n = 0; coincidenciaDeIz && n < binario.length(); n++) {
                            // Accedo a la última posición i y empiezo hacia atras.
                            int _i = matriz.length - i - 1;
                            // Accedo a la última posición j y empiezo hacia atrás.
                            int _j = matriz[_i].length - j - 1;
                            // Obtengo el valor de la posición ultima hacia atrás de la matriz
                            // como es hacia atrás, resto n iteracciones
                            int valorMInverso = matriz[_i - n][_j - n].getValor() == true ? 1 : 0;
                            if (valorMInverso != Integer.parseInt(binario.charAt(n) + "")) {
                                coincidenciaDeIz = false;
                            }
                        }
                        if (coincidenciaDeIz) {
                            cant++;
                            // Al encontrarse una coincidencia iteramos para colorear.
                            for (int n = 0; coincidenciaDeIz && n < binario.length(); n++) {
                                int _i = matriz.length - i - 1;
                                int _j = matriz[_i].length - j - 1;
                                matriz[_i - n][_j - n].setColor(true);
                            }
                        }
                    }
                }
            }
        }
        return cant;
    }

    /**
     * Función que me retorna la cantidad de veces encontrado el número en digonal
     * @param numero: número a buscar
     * @return entero con la cantidad encontrada
     */
    public int getCuantasVeces_Diagonal(int numero) {
        int cant = 0;
        // Obtenemos la cantidad diagonal de izquiera a derecha bajando y subiendo.
        cant += this.getCantidadDiagonal(this.mySopaBinaria, numero);

        // Invertimos la matriz para poder volver a llamar el método que nos busca el binario
        Bit[][] mySopaTemp = new Bit[this.mySopaBinaria.length][this.mySopaBinaria[0].length];
        // Para las busquedas diagonales no podemos usar el mismo método anterior de invertir, 
        // ya que solo necesitamos cambiar las columnas.
        for (int i = 0; i < this.mySopaBinaria.length; i++) {
            for (int j = 0; j < this.mySopaBinaria[i].length; j++) {
                int j_max = this.mySopaBinaria[i].length - 1 - j;
                // Solo cambiamos las columnas, la primera pasa a ser la última y así sucesivamente.
                mySopaTemp[i][j] = this.mySopaBinaria[i][j_max];
            }
        }

        // Volvemos a llamar el método con la matriz invertida para ver si hay binarios
        cant += this.getCantidadDiagonal(mySopaTemp, numero);

        return cant;
    }

    /**
     * Convierte número entero a binario
     *
     * @param numero: Entero, número a convertir
     * @return número convertido
     */
    public String convertirEnteroABinario(int numero) {
        if (numero <= 0) {
            return "0";
        }
        StringBuilder binario = new StringBuilder();
        while (numero > 0) {
            short residuo = (short) (numero % 2);
            numero = numero / 2;
            // Insertar el dígito al inicio de la cadena
            binario.insert(0, String.valueOf(residuo));
        }
        return binario.toString();
    }
    
    /**
     * Reset el color de la matriz cada vez que se haga una nueva búsqueda
     */
    private void resetColorMatriz() {
        for (Bit[] sopa : this.mySopaBinaria) {
            for (int j = 0; j < sopa.length; j++) {
                sopa[j].setColor(false);
            }
        }
    }
    
    /**
     * Función que ejecuta cada una de las funciones de búsqueda y el resultado lo retorna en un String
     * @param numero: entero que buscará en la sopa binaria
     * @return String con el resultado de su búsqueda
     */
    public String buscarEnSopa(int numero) {
        String binario = this.convertirEnteroABinario(numero);
        this.resetColorMatriz();
        int h = this.getCuantasVeces_Horizontal(numero);
        int v = this.getCuantasVeces_Vertical(numero);
        int d = this.getCuantasVeces_Diagonal(numero);
        return String.format("El número %d en binario es %s, se encontró %d en la sopa "
                + "de letras. "
                + "\n Horizontal: %d "
                + "\n Vertical: %d "
                + "\n Diagonal: %d \n\n", numero, binario, (h + v + d), h, v, d);
    }
    
    /**
     * Función que crea el pdf con la búsqueda realizada
     * @param numero: Entero con el número que se buscará
     * @throws Exception 
     */
    public void crearPDF(int numero) throws Exception {
        // 1. Crear el objeto para manejar el documento
        Document documento = new Document();
        //2. Crear el objeto para manejar el archivo
        FileOutputStream ficheroPdf = new FileOutputStream("src/pdf_generado.pdf");
        //2. Asociamos el fichero al documento:
        PdfWriter.getInstance(documento, ficheroPdf);
        //3. Abrimos el documento:
        documento.open();
        //Crear un parrafo:
        Paragraph parrafo = new Paragraph();
        parrafo.add(this.buscarEnSopa(numero));
        documento.add(parrafo);

        //El constructor recibe la cantidad de columnas
        PdfPTable tabla = new PdfPTable(this.mySopaBinaria[0].length);

        for (Bit[] sopa : this.mySopaBinaria) {
            //Celda:
            for (int j = 0; j < sopa.length; j++) {
                int valor = sopa[j].getValor() == true ? 1 : 0;

                PdfPCell celda = new PdfPCell(new Phrase(valor + ""));
                
                celda.setBorder(0);
                celda.setPaddingLeft(10);
                if (sopa[j].getColor()) {
                    celda.setBackgroundColor(BaseColor.YELLOW);
                }
                tabla.addCell(celda);
            }
        }
        documento.add(tabla);

        documento.close();
    }
    
    /**
     * Imprime la matriz en consola
     */
    public void imp() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        String msg = "";
        for (Bit filas[] : this.mySopaBinaria) {
            for (Bit valor : filas) {
                msg += valor.toString() + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

    public Bit[][] getMatriz() {
        return this.mySopaBinaria;
    }
}
