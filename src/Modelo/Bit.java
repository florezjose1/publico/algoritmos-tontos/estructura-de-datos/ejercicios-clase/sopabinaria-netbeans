/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Clase bit que contendrá un bit para su previa búsqueda
 * @author Jose Florez
 */
public class Bit {
    
    private boolean valor;
    // Definimos un valor más boolean para determinar si debemos colorearlo cuando coincida en las busquedas
    private boolean colorear;
    
    /**
     * Constructor vacío de la clase Bit
     */
    public Bit() {
    }
    
    /**
     * Constructor de la clase Bit con un parámetro
     * @param valor: boolean valor de verdader o falso para las busquedas
     */
    public Bit(boolean valor) {
        this.valor = valor;
    }

    /**
     * Constructor de la clase Bit con dos parámetros
     * @param valor: valor de verdader o falso
     * @param colorear: boolean para determinar si coincide con una busqueda en la sopa
     */
    public Bit(boolean valor, boolean colorear) {
        this.valor = valor;
        this.colorear = colorear;
    }

    /**
     * Método setter para insertar boolean valor al valor atributo
     * @param valor: boolean verdadero o falso como valor para la sopa
     */
    public void setValor(boolean valor) {
        this.valor = valor;
    }
    
    /**
     * Método getter de valor atributo de la claes
     * @return boolean valor
     */
    public Boolean getValor() {
        return this.valor;
    }
    
    /**
     * Método getter para determinar si se debe o no colorear el valor encontrado
     * @return boolean para saber si colorea o no.
     */
    public Boolean getColor() {
        return this.colorear;
    }
    
    /**
     * Método setter, insetar boolean de si o no colorear en la sopa.
     * @param valor boolean valor
     */
    public void setColor(boolean valor) {
        this.colorear = valor;
    }
    
    
    @Override
    public String toString() {
        //return valor == true ? "1" : "0";
        return "Bit{" + "valor=" + valor + ",color="+colorear+"}";
    }
        
}
